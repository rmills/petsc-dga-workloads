#!/usr/bin/python

# To set up environment before running this configure script, do:
#
#   export MODULEPATH=/soft/restricted/intel_dga/modulefiles:/soft/modulefiles
#   module load intel/2019 intelmpi/2019-intel
#   export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/soft/compilers/intel-2019/compilers_and_libraries/linux/mpi/intel64/libfabric/lib
#
# The last "export", above, ought to be done by the intelmpi module, but it currently isn't.

import os
petsc_hash_pkgs=os.path.join(os.getenv('HOME'),'petsc-hash-pkgs')
if not os.path.isdir(petsc_hash_pkgs): os.mkdir(petsc_hash_pkgs)

if __name__ == '__main__':
  import sys
  import os
  sys.path.insert(0, os.path.abspath('config'))
  import configure
  configure_options = [
#    '--package-prefix-hash='+petsc_hash_pkgs,
    '--with-batch=1',
    '--with-debugging=0',
    '--with-cc=mpiicc',
    '--with-cxx=mpiicpc',
    '--with-fc=mpiifort',
    '--with-memalign=64',
    '--with-mpiexec=mpiexec.hydra',
    # Note: Use -mP2OPT_hpo_vec_remainder=F for intel compilers < version 18.
    'COPTFLAGS=-g -xCORE-AVX512 -O3',
    'CXXOPTFLAGS=-g -xCORE-AVX512 -O3',
    'FOPTFLAGS=-g -xCORE-AVX512 -O3',
    '--with-avx512-kernels=1',
    '--with-blaslapack-dir='+os.environ['MKLROOT'],
#    '--download-metis=1',
#    '--download-parmetis=1',
#    '--download-superlu_dist=1'
  ]
  configure.petsc_configure(configure_options)
