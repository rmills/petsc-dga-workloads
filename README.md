# PETSc example workloads for Intel DGA

This Git repository hosts example PETSc-based workloads that can make use of MKL.
Improving support for MKL in PETSc (especially sparse BLAS routines) is a subject of active development, so this
repository will be periodically updated.
These instructions currently document only one workload, but we expect to add others as it becomes apparent which of the diverse
solvers PETSc supports may be most useful to work with.

For questions, please contact Richard Tran Mills (<rtmills@anl.gov>).

## Preliminaries: Installing PETSc

Clone the PETSc source code PETSc development repository and, optionally, checkout commit `421228721c`:

```bash
git clone https://gitlab.com/petsc/petsc.git
git checkout 421228721c
```

As of this writing, this benchmark works as intended with the `main` (previously known as `master`) branch, so it is possible to simply use that to work with
the latest version of PETSc, if preferred.

PETSc installation instructions can be found at https://www.mcs.anl.gov/petsc/documentation/installation.html.
PETSc should be configured, of course, to use Intel MKL to supply BLAS and LAPACK routines.
To build PETSc for JLSE Skylake nodes, set `PETSC_DIR` to the top level of your PETSc clone, and, from the top level of
the petsc-dga-workloads working copy, do

```bash
cp config/arch-jlse-skylake-intel-opt.py $PETSC_DIR/config/.
cd $PETSC_DIR
export MODULEPATH=/soft/restricted/intel_dga/modulefiles:/soft/modulefiles
module load intel/2019 intelmpi/2019-intel
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/soft/compilers/intel-2019/compilers_and_libraries/linux/mpi/intel64/libfabric/lib
config/arch-jlse-skylake-intel-opt.py
export PETSC_ARCH=arch-jlse-skylake-intel-opt
make all
```

## Running the workloads

### SNES ex56 GAMG example

This example solves a 3D elasticity problem using a tri-quadratic hexahedra (Q1), displacement finite element formulation.
A wide variety of PETSc solvers can run using the ex56 executable, but we use an outer, conjugate gradient solver preconditioned
using the PCGAMG component of PETSc, which here performs smoothed aggregation algebraic multigrid and uses Chebyshev polynomial
smoothers.
This workload uses MKL to perform BLAS vector operations (e.g., DDOT, DAXPY, DSCAL), as well as QR and LU factorizations on small
matrices.
If run using the MATAIJMKL matrix type in PETSc, it also uses inspector-executor sparse BLAS operations from MKL to perform
matrix-vector multiplication and sparse matrix-matrix multiplication (in matrix triple products used to construct coarsened
operators via Galerkin projection).
MKL sparse BLAS may not be used everywhere that it is appropriate; we are actively working to make sure to leverage MKL for this
whenever possible, so this may improve in the near future.

With `PETSC_DIR` and `PETSC_ARCH` set appropriately to indicate the PETSc installation to use, build and run the example (without
using MKL sparse BLAS routines) by doing

```bash
cd $PETSC_DIR/src/snes/tutorials
make ex56
mpirun -n 56 ./ex56 -cells 8,12,16 -check_pointer_intensity 0 -ex56_dm_refine 3 -pc_gamg_esteig_ksp_max_it 10 -pc_gamg_esteig_ksp_type cg -ksp_atol 1.e-71 -ksp_converged_reason -ksp_monitor -ksp_norm_type unpreconditioned -ksp_rtol 1.e-16 -ksp_type cg -mat_block_size 3 -matptap_via scalable -max_conv_its 1 -mg_levels_esteig_ksp_max_it 10 -mg_levels_esteig_ksp_type cg -mg_levels_ksp_chebyshev_esteig 0,0.05,0,1.05 -mg_levels_ksp_max_it 2 -mg_levels_ksp_type chebyshev -mg_levels_pc_type jacobi -options_left -pc_gamg_agg_nsmooths 1 -pc_gamg_coarse_eq_limit 100 -pc_gamg_cpu_pin_coarse_grids -pc_gamg_process_eq_limit 100 -pc_gamg_repartition false -pc_gamg_reuse_interpolation true -pc_gamg_square_graph 10 -pc_gamg_threshold 0.05 -pc_gamg_threshold_scale .5 -pc_gamg_type agg -pc_gamg_coarse_grid_layout_type spread -pc_type gamg -petscpartitioner_type simple -petscspace_degree 1 -run_type 1 -snes_converged_reason -snes_max_it 1 -snes_monitor -snes_rtol 1.e-10 -snes_type ksponly -use_mat_nearnullspace true -pc_mg_log -log_view
```

varying the number of MPI ranks as appropriate for the target system; `-n 56` is appropriate to the 56-core Skylake nodes in JLSE.
The `-snes_monitor -ksp_monitor -pc_mg_log -log_view` options result in a fair amount of console output, so it may be a desirable
to record the console output by appending something such as `2>&1 | tee ex56-n56-out.txt` to the command line.

If successful, the console output should be similar to that contained in [ex56-n56-out.txt](./example_output/ex56-n56-out.txt).
Slight numerical differences will result on different platforms, but the number of KSP iterations should be about 34, and
successful convergence should be evidenced by output like

```
Linear solve converged due to CONVERGED_RTOL iterations 34
```

**To use PETSc's support for MKL sparse BLAS**, add `-mat_seqaij_type seqaijmkl` to the command line.
Unfortunately, the sparse BLAS routines are not currently reported using `MKL_VERBOSE`.
Richard will soon add an option to turn on logging this information inside PETSc, and will update these instructions
when this is ready.

**As an alternative to using the very long command lines above**, text files containing the desired options can be used.
If a copy of the `options/ex56-options.txt` file in this repo is copied to the working directory, the above example can also be
run via

```bash
mpirun -n 56 ./ex56 -options_file ./ex56-options.txt
```

and running the same example using the support for MKL sparse BLAS can be done via

```bash
mpirun -n 56 ./ex56 -options_file ./ex56-aijmkl-options.txt
```
